# Herbivorous
* Horse
* Cow
* Sheep
* Chicken

# Carnivorous
* Polar Bear
* Frog
* Hawk
* Lion
* Capybara

# Omnivorous
* Cat
* Dog
* Rat
* Fox
* Raccoon
* Fennec Fox